# Pandora money box

## Description

Répertoire crowdsourcé de sources de financement, avec extraction des calendriers
et modalités.

## Accéder aux données

Le cœur du sujet : >> **[Ouvrir le tableur de référencement collaboratif](https://lite.framacalc.org/pandora-money-box-list)** <<

## Sauvegarde du tableur collaboratiff

Sauvegarde des données : [CSV de sauvegarde](liste-de-sources-de-financement.csv)

(sauvegarder les données : uploader https://lite.framacalc.org/pandora-money-box-list.csv
comme nouvelle version)